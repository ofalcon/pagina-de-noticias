import React, {Fragment, useState, useEffect} from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import ListadoNoticias from './components/ListadoNoticias'
import './components/App-style.css';





function App() {
//definir la categoria y noticias
  const [categoria, guardarCategoria] = useState('');
  const [noticias, guardarNoticias] = useState([])

  useEffect(() => {
const consultarAPI = async () => {
  const url =`http://newsapi.org/v2/top-headlines?country=ar&category=${categoria}&apiKey=a74797abd9524c51ac2a99c67b103ad4`;

  const respuesta = await fetch(url);
  const noticias = await respuesta.json();

 guardarNoticias(noticias.articles)
}
  consultarAPI();
}, [categoria]);

  return (
    <Fragment>
      <Header
      titulo="Noticias de Argentina"/>

      <div className="container white"> 
            <Formulario
            guardarCategoria={guardarCategoria}/>

            <ListadoNoticias
            noticias={noticias}/>
      </div>
    </Fragment>
  );
}

export default App;
